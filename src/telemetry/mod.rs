//! atlas telemetry
use error::{ErrorKind, Result};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::{Data, Request, Response};
use rusqlite::Connection;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::sync::RwLock;
use std::time::Instant;
use utils::{stdout, MsgKind};
use uuid::Uuid;

const ATLAS_UUID_HEADER: &str = "X-Atlas-Uuid";

const TELEMETRY_CREATE: &str = r"
CREATE TABLE IF NOT EXISTS ATLAS_TELEM (
    ID        INTEGER PRIMARY KEY,
    UUID      TEXT NOT NULL,
    METHOD    TEXT NOT NULL,
    URI       TEXT NOT NULL,
    ELAPSED   REAL NOT NULL,
    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
)";

const TELEMETRY_INSERT: &str = r"
INSERT INTO ATLAS_TELEM
(UUID, METHOD, URI, ELAPSED)
VALUES
(?1, ?2, ?3, ?4)
";

#[derive(Default)]
pub struct Telemetry {
    start: RwLock<HashMap<Uuid, Instant>>,
}

impl Telemetry {
    fn request(&self, req: &mut Request, _: &Data) -> Result<()> {
        let mut timing_map = self.start.write()?;
        let uuid = Uuid::new_v4();
        let header = Header::new(ATLAS_UUID_HEADER, uuid.hyphenated().to_string());
        req.add_header(header);
        timing_map.insert(uuid, Instant::now());
        Ok(())
    }

    fn response(&self, req: &Request, _: &mut Response) -> Result<()> {
        let uuid_header = req
            .headers()
            .get_one(ATLAS_UUID_HEADER)
            .ok_or_else(|| ErrorKind::Header)?;
        let uuid = Uuid::parse_str(uuid_header)?;
        let mut timing_map = self.start.write()?;
        if let Some(start) = timing_map.remove(&uuid) {
            let conn = Connection::open("telemetry")?;
            conn.execute(TELEMETRY_CREATE, &[])?;
            let method = req.method();
            let uri = req.uri();
            let elapsed_dur = start.elapsed();
            let secs: u32 = TryFrom::try_from(elapsed_dur.as_secs())?;
            let elapsed = f64::from(secs) + f64::from(elapsed_dur.subsec_nanos()) * 1e-9;
            let msg = format!("{} {} {} {}", uuid, method, uri, elapsed);
            conn.execute(
                TELEMETRY_INSERT,
                &[
                    &uuid_header,
                    &method.to_string(),
                    &uri.to_string(),
                    &elapsed,
                ],
            )?;
            stdout(&MsgKind::Telemetry, &msg)?;
        }
        Ok(())
    }
}

impl Fairing for Telemetry {
    fn info(&self) -> Info {
        Info {
            name: "Atlas Telemetry",
            kind: Kind::Request | Kind::Response,
        }
    }

    fn on_request(&self, req: &mut Request, data: &Data) {
        let _ = self.request(req, data);
    }

    fn on_response(&self, req: &Request, res: &mut Response) {
        let _ = self.response(req, res);
    }
}
