// Copyright (c) 2017 atlas developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atlas` runtime
use clap::{App, Arg};
use cors::CORS;
use db::{self, user};
use dotenv;
use error::{ErrorKind, Result};
use libc;
use model::Env;
use nix::sys::signal;
use rocket;
use routes::{auths, opts, qrcodes, roles};
// use sloggers::Build;
// use sloggers::file::FileLoggerBuilder;
// use sloggers::types::Severity;
use std::borrow::Borrow;
use std::convert::TryFrom;
use std::path::PathBuf;
use telemetry::Telemetry;
use term;

fn env_header(filename: &PathBuf) -> Result<()> {
    let mut t = term::stdout().ok_or_else(|| ErrorKind::Stdout)?;
    t.fg(term::color::BLUE)?;
    writeln!(t, "\u{01F527}  Loaded env from {}", filename.display())?;
    t.flush()?;
    t.reset()?;
    Ok(())
}

extern "C" fn handle_sigint(blah: i32) {
    unsafe {
        libc::exit(blah);
    }
}

/// CLI Runtime
pub fn run() -> Result<i32> {
    let sig_action = signal::SigAction::new(
        signal::SigHandler::Handler(handle_sigint),
        signal::SaFlags::empty(),
        signal::SigSet::empty(),
    );
    unsafe {
        signal::sigaction(signal::SIGINT, &sig_action)?;
    }

    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Run the Atlas API")
        .arg(
            Arg::with_name("env")
                .long("env")
                .value_name("ENV")
                .help("Set the runtime environment")
                .possible_values(&["dev", "stage", "prod"])
                .default_value("dev")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("refresh")
                .long("refresh")
                .short("r")
                .help("Refresh the database on startup (only valid for dev env)"),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let env: Env = TryFrom::try_from(matches.value_of("env"))?;
    let mut filename = PathBuf::from("env");
    filename.push(Into::<String>::into(env.clone()));
    filename.set_extension("env");

    dotenv::from_filename(filename.to_string_lossy().borrow())?;
    env_header(&filename)?;

    // let severity = match matches.occurrences_of("v") {
    //     0 => Severity::Warning,
    //     1 => Severity::Info,
    //     2 => Severity::Debug,
    //     3 | _ => Severity::Trace,
    // };

    // let mut builder = FileLoggerBuilder::new("atlas.log");
    // builder.level(severity);
    // let log = builder.build()?;

    if env == Env::Dev && matches.is_present("refresh") {
        user::drop_atlas_tables()?;
        user::create_atlas_tables()?;
        user::insert_atlas_test_data()?;
    }

    let telemetry: Telemetry = Default::default();

    rocket::ignite()
        .attach(CORS {})
        .attach(telemetry)
        .manage(db::init_pool()?)
        .manage(env)
        // .manage(log)
        .mount("/api/v1", opts::catch_all_options_routes())
        .mount(
            "/api/v1",
            routes![
                auths::post,
                auths::patch_plain,
                auths::patch_tfa,
                qrcodes::get,
                roles::get,
                roles::post,
                roles::put,
                roles::patch,
                roles::delete,
                roles::get_by_id,
            ],
        )
        .launch();

    Ok(0)
}
