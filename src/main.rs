// Copyright (c) 2017 atlas developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atlas` 0.1.0
#![deny(missing_docs)]
#![feature(decl_macro, plugin, try_from)]
#![plugin(rocket_codegen)]
#![cfg_attr(feature = "cargo-clippy", allow(missing_docs_in_private_items))]
#![recursion_limit = "128"]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate getset;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate slog;

extern crate argon2rs;
extern crate base32;
extern crate chrono;
extern crate clap;
extern crate dotenv;
extern crate image;
extern crate jsonwebtoken;
extern crate libc;
extern crate mimir;
extern crate nix;
extern crate oath;
extern crate qrcode;
extern crate rand;
extern crate rocket;
extern crate rocket_contrib;
extern crate rusqlite;
// extern crate sloggers;
extern crate term;
extern crate uuid;

mod cors;
mod db;
#[allow(unused_doc_comment)]
mod error;
mod model;
mod routes;
mod run;
mod telemetry;
mod utils;

use std::io::{self, Write};
use std::process;

/// CLI Entry Point
fn main() {
    match run::run() {
        Ok(i) => process::exit(i),
        Err(e) => {
            writeln!(io::stderr(), "{}", e).expect("Unable to write to stderr!");
            process::exit(1)
        }
    }
}
