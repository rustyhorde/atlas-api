//! Utility functions
use error::{ErrorKind, Result};
use jsonwebtoken::{decode, Algorithm, TokenData, Validation};
use model::Claims;
use std::{env, fmt};
use term;

pub fn get_token_data_from_header(header: &str) -> Result<TokenData<Claims>> {
    if header.starts_with("Bearer") {
        let toks: Vec<&str> = header.split(' ').collect();
        if toks.len() == 2 {
            let secret = env::var("JWT_SECRET")?;
            let validation = Validation {
                algorithms: vec![Algorithm::HS512],
                leeway: 30,
                ..Default::default()
            };
            Ok(decode::<Claims>(toks[1], secret.as_bytes(), &validation)?)
        } else {
            Err(ErrorKind::InvalidJwt.into())
        }
    } else {
        Err(ErrorKind::InvalidAuthHeader.into())
    }
}

pub fn to_hex_str(bytes: &[u8; 32]) -> String {
    let bytes_vec: Vec<String> = bytes.iter().map(|b| format!("{:02x}", b)).collect();
    bytes_vec.join("")
}

pub enum MsgKind {
    Security,
    Telemetry,
}

impl fmt::Display for MsgKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let unicode = match *self {
            MsgKind::Security => "\u{1f512} ",
            MsgKind::Telemetry => "\u{1f951} ",
        };
        write!(f, "{}", unicode)
    }
}

pub fn stdout(msg_kind: &MsgKind, msg: &str) -> Result<()> {
    let mut t = term::stdout().ok_or_else(|| ErrorKind::Stdout)?;
    write!(t, "    => ")?;
    t.fg(term::color::BLUE)?;
    writeln!(t, "{} {}", msg_kind, msg)?;
    t.flush()?;
    t.reset()?;
    Ok(())
}
