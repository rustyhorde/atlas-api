// Copyright (c) 2017 atlas developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.
//! `atlas` errors
use rocket::http::Status;
use rocket::request::Request;
use rocket::response::{Responder, Response};

error_chain! {
    foreign_links {
        Dotenv(::dotenv::Error);
        Env(::std::env::VarError);
        Io(::std::io::Error);
        Jsonwebtoken(::jsonwebtoken::errors::Error);
        Mimir(::mimir::error::Error);
        Nix(::nix::Error);
        ParseInt(::std::num::ParseIntError);
        ParseUuid(::uuid::ParseError);
        Rusqlite(::rusqlite::Error);
        // Sloggers(::sloggers::Error);
        Term(::term::Error);
        TryFromInt(::std::num::TryFromIntError);
    }

    errors {
        Auth {
            description("Unable to authentication user!")
            display("Unable to authentication user!")
        }
        Decode {
            description("")
            display("")
        }
        Header {
            description("")
            display("")
        }
        InvalidAction {
            description("Action not supported at the given route!")
            display("Action not supported at the given route!")
        }
        InvalidAuthHeader {
            description("An invalid authorization header was supplied!")
            display("An invalid authorization header was supplied!")
        }
        InvalidEnv(env: String) {
            description("Invalid environment specified!")
            display("Invalid environment '{}' specified!", env)
        }
        InvalidJwt {
            description("Unable to decode the given JWT!")
            display("Unable to decode the given JWT!")
        }
        InvalidNativeType {
            description("")
            display("")
        }
        InvalidVar {
            description("")
            display("")
        }
        NotImplemented {
            description("The requested API is not implemented!")
            display("The requested API is not implemented!")
        }
        QrCode {
            description("")
            display("")
        }
        Stdout {
            description("Unable to get stdout terminal!")
            display("Unable to get stdout terminal!")
        }
        SyncPoisonError(msg: String) {
            description("")
            display("")
        }
        TfaNotFound {
            description("Two-Factor Authentication key not found!")
            display("Two-Factor Authentication key not found!")
        }
        TfaRequired {
            description("Two-Factor Authentication has not been completed.")
            display("Two-Factor Authentication has not been completed.")
        }
        TooManyResults {
            description("")
            display("")
        }
    }
}

impl Responder<'static> for Error {
    fn respond_to(self, _: &Request) -> ::std::result::Result<Response<'static>, Status> {
        match self.0 {
            ErrorKind::Auth => Err(Status::Unauthorized),
            ErrorKind::InvalidAction | ErrorKind::Mimir(_) | ErrorKind::TfaNotFound => {
                Err(Status::BadRequest)
            }
            ErrorKind::NotImplemented => Err(Status::NotImplemented),
            _ => Err(Status::NotFound),
        }
    }
}

impl<T> From<::std::sync::PoisonError<T>> for Error {
    fn from(err: ::std::sync::PoisonError<T>) -> Self {
        use std::error::Error;

        Self::from_kind(ErrorKind::SyncPoisonError(err.description().to_string()))
    }
}
