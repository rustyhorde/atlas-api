//! `atlas` database connection pool
use error::Result;
use mimir::flags::DPI_MODE_CREATE_THREADED;
use mimir::{Context, Pool};
use std::env;

pub mod role;
pub mod user;

pub fn init_pool() -> Result<Pool> {
    let username = env::var("ATLAS_USERNAME")?;
    let password = env::var("ATLAS_PASSWORD")?;
    let url = env::var("ATLAS_CONN_STRING")?;
    let ctxt = Context::create()?;

    let mut common_create_params = ctxt.init_common_create_params()?;
    common_create_params.set_encoding("UTF-8")?;
    common_create_params.set_nchar_encoding("UTF-8")?;
    common_create_params.set_create_mode(DPI_MODE_CREATE_THREADED);

    let mut pool_create_params = ctxt.init_pool_create_params()?;
    pool_create_params.set_min_sessions(10);
    pool_create_params.set_max_sessions(100);
    pool_create_params.set_session_increment(5);

    let pool = Pool::create(
        &ctxt,
        Some(&username),
        Some(&password),
        Some(&url),
        Some(common_create_params),
        Some(pool_create_params),
    )?;

    Ok(pool)
}
