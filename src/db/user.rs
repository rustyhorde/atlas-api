//! `user` table queries
use error::{ErrorKind, Result};
use mimir::enums::ODPINativeTypeNum::{Bytes, Uint64};
use mimir::enums::ODPIOracleTypeNum::{Number, Varchar};
use mimir::flags::DPI_MODE_CREATE_THREADED;
use mimir::{flags, Connection, Context, Pool};
use std::env;

const DROP_ATLAS_USER_SEQ: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP SEQUENCE ATLAS_USER_SEQ';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -2289 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_USER_INDEX: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP INDEX ATLAS_USER_AUTH_HASH_IDX';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -1418 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_USER: &str = r"
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE ATLAS_USER';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;";

const DROP_ATLAS_ROLE_SEQ: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP SEQUENCE ATLAS_ROLE_SEQ';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -2289 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_ROLE_INDEX: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP INDEX ATLAS_ROLE_ROLE_NAME_IDX';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -1418 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_ROLE: &str = r"
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE ATLAS_ROLE';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;";

const DROP_ATLAS_USER_ROLE_SEQ: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP SEQUENCE ATLAS_USER_ROLE_SEQ';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -2289 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_USER_ROLE_INDEX: &str = r"
BEGIN
  EXECUTE IMMEDIATE 'DROP INDEX ATLAS_USER_ROLE_UID_RID_IDX';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE != -1418 THEN
      RAISE;
    END IF;
END;";

const DROP_ATLAS_USER_ROLE: &str = r"
BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE ATLAS_USER_ROLE';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;";

const CREATE_ATLAS_USER_SEQ: &str = r"CREATE SEQUENCE ATLAS_USER_SEQ";

const CREATE_ATLAS_USER_TABLE: &str = r"
CREATE TABLE ATLAS_USER (
    ID NUMBER NOT NULL,
    AUTH_HASH VARCHAR2(64) NOT NULL,
    HONORIFIC VARCHAR(255),
    FORENAME VARCHAR2(255) NOT NULL,
    SURNAME VARCHAR2(255) NOT NULL,
    TFA_SK VARCHAR(64),
    QR_CODE_FILE VARCHAR(64),
    CONSTRAINT ATLAS_USER_PK_ID PRIMARY KEY(ID)
)";

const CREATE_ATLAS_USER_INDEX: &str = r"
CREATE UNIQUE INDEX ATLAS_USER_AUTH_HASH_IDX ON ATLAS_USER (AUTH_HASH)";

const CREATE_ATLAS_ROLE_SEQ: &str = r"CREATE SEQUENCE ATLAS_ROLE_SEQ";

const CREATE_ATLAS_ROLE_TABLE: &str = r"
CREATE TABLE ATLAS_ROLE (
    ID NUMBER NOT NULL,
    ROLENAME VARCHAR(255) NOT NULL,
    READ_USERS NUMBER(1) DEFAULT 0 NOT NULL,
    CREATE_USER NUMBER(1) DEFAULT 0 NOT NULL,
    READ_USER NUMBER(1) DEFAULT 0 NOT NULL,
    UPDATE_USER NUMBER(1) DEFAULT 0 NOT NULL,
    DELETE_USER NUMBER(1) DEFAULT 0 NOT NULL,
    CREATE_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    READ_ROLES NUMBER(1) DEFAULT 0 NOT NULL,
    READ_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    UPDATE_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    DELETE_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    READ_USER_ROLES NUMBER(1) DEFAULT 0 NOT NULL,
    CREATE_USER_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    READ_USER_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    UPDATE_USER_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    DELETE_USER_ROLE NUMBER(1) DEFAULT 0 NOT NULL,
    CONSTRAINT ATLAS_ROLE_PK_ID PRIMARY KEY(ID)
)";

const CREATE_ATLAS_ROLE_INDEX: &str = r"
CREATE UNIQUE INDEX ATLAS_ROLE_ROLE_NAME_IDX ON ATLAS_ROLE (ROLENAME)";

const CREATE_ATLAS_USER_ROLE_SEQ: &str = r"CREATE SEQUENCE ATLAS_USER_ROLE_SEQ";

const CREATE_ATLAS_USER_ROLE_TABLE: &str = r"
CREATE TABLE ATLAS_USER_ROLE (
    ID NUMBER NOT NULL,
    USER_ID NUMBER NOT NULL,
    ROLE_ID NUMBER NOT NULL,
    CONSTRAINT ATLAS_USER_ROLE_PK_ID PRIMARY KEY(ID),
    CONSTRAINT FK_USER
        FOREIGN KEY (USER_ID)
        REFERENCES ATLAS_USER (ID),
    CONSTRAINT FK_ROLE
        FOREIGN KEY (ROLE_ID)
        REFERENCES ATLAS_ROLE (ID)
)";

const CREATE_ATLAS_USER_ROLE_INDEX: &str = r"
CREATE UNIQUE INDEX ATLAS_USER_ROLE_UID_RID_IDX ON ATLAS_USER_ROLE (USER_ID, ROLE_ID)";

const INSERT_ATLAS_USER: &str = r"
INSERT INTO ATLAS_USER
(ID, AUTH_HASH, FORENAME, SURNAME, TFA_SK, QR_CODE_FILE)
VALUES
(ATLAS_USER_SEQ.NEXTVAL, :authhash, :forename, :surname, :tfask, :qrfile)";

const INSERT_ATLAS_USER_NO_TFA: &str = r"
INSERT INTO ATLAS_USER
(ID, AUTH_HASH, FORENAME, SURNAME)
VALUES
(ATLAS_USER_SEQ.NEXTVAL, :authhash, :forename, :surname)
";

const INSERT_ATLAS_ADMIN_ROLE: &str = r"
INSERT INTO ATLAS_ROLE
VALUES
(ATLAS_ROLE_SEQ.NEXTVAL, 'Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
";

const INSERT_ATLAS_BASE_ROLE: &str = r"
INSERT INTO ATLAS_ROLE
(ID, ROLENAME, READ_USER)
VALUES
(ATLAS_ROLE_SEQ.NEXTVAL, 'Base', 1)
";

const INSERT_ATLAS_ADMIN_ATLAS_USER_ROLE: &str = r"
INSERT INTO ATLAS_USER_ROLE
(ID, USER_ID, ROLE_ID)
VALUES
(ATLAS_USER_ROLE_SEQ.NEXTVAL, :user_id, :role_id)
";

const FIND_USER_BY_AUTH_HASH: &str = r"
SELECT ID, TFA_SK
FROM ATLAS_USER
WHERE AUTH_HASH = :authhash
";

const UPDATE_ENABLE_TFA: &str = r"
UPDATE ATLAS_USER
SET TFA_SK       = :tfask,
    QR_CODE_FILE = :qrfile
WHERE AUTH_HASH  = :authhash
AND   ID         = :id
";

const FIND_TFA_SK_BY_AUTH_HASH: &str = r"
SELECT TFA_SK
FROM ATLAS_USER AU
WHERE AU.AUTH_HASH = :authhash
AND   AU.ID        = :atlasid
";

fn execute_stmt(conn: &Connection, stmt: &str) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(stmt), None, false)?;
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

    Ok(())
}

pub fn drop_atlas_tables() -> Result<()> {
    let username = env::var("ATLAS_USERNAME")?;
    let password = env::var("ATLAS_PASSWORD")?;
    let url = env::var("ATLAS_CONN_STRING")?;
    let ctxt = Context::create()?;

    let mut ccp = ctxt.init_common_create_params()?;
    ccp.set_encoding("UTF-8")?;
    ccp.set_nchar_encoding("UTF-8")?;
    ccp.set_create_mode(DPI_MODE_CREATE_THREADED);

    let conn = Connection::create(
        &ctxt,
        Some(&username),
        Some(&password),
        Some(&url),
        Some(ccp),
        None,
    )?;

    execute_stmt(&conn, DROP_ATLAS_USER_ROLE_SEQ)
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_USER_ROLE_INDEX))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_USER_ROLE))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_USER_SEQ))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_USER_INDEX))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_USER))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_ROLE_SEQ))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_ROLE_INDEX))
        .and_then(|_| execute_stmt(&conn, DROP_ATLAS_ROLE))?;

    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    Ok(())
}

pub fn create_atlas_tables() -> Result<()> {
    let username = env::var("ATLAS_USERNAME")?;
    let password = env::var("ATLAS_PASSWORD")?;
    let url = env::var("ATLAS_CONN_STRING")?;
    let ctxt = Context::create()?;

    let mut ccp = ctxt.init_common_create_params()?;
    ccp.set_encoding("UTF-8")?;
    ccp.set_nchar_encoding("UTF-8")?;
    ccp.set_create_mode(DPI_MODE_CREATE_THREADED);

    let conn = Connection::create(
        &ctxt,
        Some(&username),
        Some(&password),
        Some(&url),
        Some(ccp),
        None,
    )?;

    execute_stmt(&conn, CREATE_ATLAS_USER_SEQ)
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_USER_TABLE))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_USER_INDEX))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_ROLE_SEQ))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_ROLE_TABLE))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_ROLE_INDEX))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_USER_ROLE_SEQ))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_USER_ROLE_TABLE))
        .and_then(|_| execute_stmt(&conn, CREATE_ATLAS_USER_ROLE_INDEX))?;

    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    Ok(())
}

pub fn insert_atlas_test_data() -> Result<()> {
    let username = env::var("ATLAS_USERNAME")?;
    let password = env::var("ATLAS_PASSWORD")?;
    let url = env::var("ATLAS_CONN_STRING")?;
    let ctxt = Context::create()?;

    let mut ccp = ctxt.init_common_create_params()?;
    ccp.set_encoding("UTF-8")?;
    ccp.set_nchar_encoding("UTF-8")?;
    ccp.set_create_mode(DPI_MODE_CREATE_THREADED);

    let conn = Connection::create(
        &ctxt,
        Some(&username),
        Some(&password),
        Some(&url),
        Some(ccp),
        None,
    )?;

    conn.begin_distrib_trans(1, "ATLAS", "ATLAS_USER_INSERTS")?;

    insert_atlas_user_test(&conn)
        .and_then(|_| insert_atlas_user_no_tfa_test(&conn))
        .and_then(|_| insert_atlas_admin_role(&conn))
        .and_then(|_| insert_atlas_base_role(&conn))
        .and_then(|_| insert_atlas_admin_atlas_user_role(&conn))?;

    if conn.prepare_distrib_trans()? {
        conn.commit()?;
    }
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    Ok(())
}

pub fn insert_atlas_admin_role(conn: &Connection) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(INSERT_ATLAS_ADMIN_ROLE), None, false)?;
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
    Ok(())
}

pub fn insert_atlas_base_role(conn: &Connection) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(INSERT_ATLAS_BASE_ROLE), None, false)?;
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
    Ok(())
}

pub fn insert_atlas_user_test(conn: &Connection) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(INSERT_ATLAS_USER), None, false)?;

    // setup the forename bind
    let forename_var = conn.new_var(Varchar, Bytes, 1, 255, false, false)?;
    forename_var.set_from_bytes(0, &env::var("ATLAS_USER_1_FORENAME")?)?;
    stmt.bind_by_name(":forename", &forename_var)?;

    // setup the surname bind
    let surname_var = conn.new_var(Varchar, Bytes, 1, 255, false, false)?;
    surname_var.set_from_bytes(0, &env::var("ATLAS_USER_1_SURNAME")?)?;
    stmt.bind_by_name(":surname", &surname_var)?;

    // setup the tfa_sk bind
    let tfa_sk_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
    tfa_sk_var.set_from_bytes(0, &env::var("ATLAS_USER_1_TFASK")?)?;
    stmt.bind_by_name(":tfask", &tfa_sk_var)?;

    // setup the qrfile bind
    let qr_file = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
    qr_file.set_from_bytes(0, &env::var("ATLAS_USER_1_QRFILE")?)?;
    stmt.bind_by_name(":qrfile", &qr_file)?;

    // setup the authhash bind
    let auth_hash_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
    auth_hash_var.set_from_bytes(0, &env::var("ATLAS_USER_1_AUTHHASH")?)?;
    stmt.bind_by_name(":authhash", &auth_hash_var)?;

    // execute and fetch results
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

    Ok(())
}

pub fn insert_atlas_user_no_tfa_test(conn: &Connection) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(INSERT_ATLAS_USER_NO_TFA), None, false)?;

    // setup the forename bind
    let forename_var = conn.new_var(Varchar, Bytes, 1, 255, false, false)?;
    forename_var.set_from_bytes(0, &env::var("ATLAS_USER_2_FORENAME")?)?;
    stmt.bind_by_name(":forename", &forename_var)?;

    // setup the surname bind
    let surname_var = conn.new_var(Varchar, Bytes, 1, 255, false, false)?;
    surname_var.set_from_bytes(0, &env::var("ATLAS_USER_2_SURNAME")?)?;
    stmt.bind_by_name(":surname", &surname_var)?;

    // setup the authhash bind
    let auth_hash_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
    auth_hash_var.set_from_bytes(0, &env::var("ATLAS_USER_2_AUTHHASH")?)?;
    stmt.bind_by_name(":authhash", &auth_hash_var)?;

    // execute and fetch results
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

    Ok(())
}

pub fn insert_atlas_admin_atlas_user_role(conn: &Connection) -> Result<()> {
    let stmt = conn.prepare_stmt(Some(INSERT_ATLAS_ADMIN_ATLAS_USER_ROLE), None, false)?;

    // setup the user_id binds.
    let user_id_var = conn.new_var(Number, Uint64, 3, 0, false, false)?;
    let user_id_data = user_id_var.get_data()?;
    for (idx, data) in user_id_data.iter_mut().enumerate() {
        (*data).is_null = 0;

        match idx {
            0 | 1 => (*data).value.as_uint_64 = 1,
            2 => (*data).value.as_uint_64 = 2,
            _ => {}
        }
    }
    stmt.bind_by_pos(1, &user_id_var)?;

    // setup the role_id binds.
    let role_id_var = conn.new_var(Number, Uint64, 3, 0, false, false)?;
    let role_id_data = role_id_var.get_data()?;
    for (idx, data) in role_id_data.iter_mut().enumerate() {
        (*data).is_null = 0;

        match idx {
            0 => (*data).value.as_uint_64 = 1,
            1 | 2 => (*data).value.as_uint_64 = 2,
            _ => {}
        }
    }
    stmt.bind_by_pos(2, &role_id_var)?;

    stmt.execute_many(flags::DPI_MODE_EXEC_DEFAULT, 3)?;

    Ok(())
}

pub fn find_user_by_auth_hash(pool: &Pool, auth_hash: &str) -> Result<(u64, bool)> {
    let res: (u64, bool);
    let conn = pool.acquire_connection(None, None, None)?;

    {
        let stmt = conn.prepare_stmt(Some(FIND_USER_BY_AUTH_HASH), None, false)?;
        let auth_hash_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
        auth_hash_var.set_from_bytes(0, auth_hash)?;
        stmt.bind_by_name(":authhash", &auth_hash_var)?;
        stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
        stmt.define_value(1, Number, Uint64, None, None)?;
        stmt.fetch()?;

        let (_id_type, id_data) = stmt.get_query_value(1)?;
        let (_tfa_sk_type, tfa_sk_data) = stmt.get_query_value(2)?;
        let tfa_sk = tfa_sk_data.get_string();
        res = (id_data.get_uint64(), !tfa_sk.is_empty());
    }

    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;
    Ok(res)
}

pub fn enable_tfa(pool: &Pool, id: u64, auth_hash: &str, key: &str, filename: &str) -> Result<()> {
    let conn = pool.acquire_connection(None, None, None)?;
    {
        let stmt = conn.prepare_stmt(Some(UPDATE_ENABLE_TFA), None, false)?;

        // setup the tfa_sk bind
        let tfa_sk_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
        tfa_sk_var.set_from_bytes(0, key)?;
        stmt.bind_by_name(":tfask", &tfa_sk_var)?;

        // setup the qrfile bind
        let qr_file = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
        qr_file.set_from_bytes(0, filename)?;
        stmt.bind_by_name(":qrfile", &qr_file)?;

        // setup the authhash bind
        let auth_hash_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
        auth_hash_var.set_from_bytes(0, auth_hash)?;
        stmt.bind_by_name(":authhash", &auth_hash_var)?;

        // setup the id bind
        let id_var = conn.new_var(Number, Uint64, 1, 0, false, false)?;
        let id_data = id_var.get_data()?;

        if id_data.len() == 1 {
            id_data[0].is_null = 0;
            id_data[0].value.as_uint_64 = id;
            stmt.bind_by_name(":atlasid", &id_var)?;

            // execute and fetch results
            stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
        } else {
            return Err(ErrorKind::InvalidNativeType.into());
        }
    }

    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;
    Ok(())
}

pub fn get_tfa_sk(pool: &Pool, id: u64, auth_hash: &str) -> Result<String> {
    let res: String;
    let conn = pool.acquire_connection(None, None, None)?;
    {
        let stmt = conn.prepare_stmt(Some(FIND_TFA_SK_BY_AUTH_HASH), None, false)?;

        // setup the authhash bind
        let auth_hash_var = conn.new_var(Varchar, Bytes, 1, 64, false, false)?;
        auth_hash_var.set_from_bytes(0, auth_hash)?;
        stmt.bind_by_name(":authhash", &auth_hash_var)?;

        // setup the id bind
        let id_var = conn.new_var(Number, Uint64, 1, 0, false, false)?;
        let id_data = id_var.get_data()?;

        if id_data.len() == 1 {
            id_data[0].is_null = 0;
            id_data[0].value.as_uint_64 = id;
            stmt.bind_by_name(":atlasid", &id_var)?;

            stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
            stmt.fetch()?;

            let (tfa_sk_type, tfa_sk_data) = stmt.get_query_value(1)?;
            if tfa_sk_type == Bytes {
                res = tfa_sk_data.get_string();
            } else {
                return Err(ErrorKind::TfaNotFound.into());
            }
        } else {
            return Err(ErrorKind::InvalidNativeType.into());
        }
    }
    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;
    Ok(res)
}
