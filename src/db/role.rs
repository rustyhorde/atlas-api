//! `Role` queries
use error::{ErrorKind, Result};
use mimir::enums::ODPIFetchMode::{First, Next};
use mimir::enums::ODPINativeTypeNum::Uint64;
use mimir::enums::ODPIOracleTypeNum::Number;
use mimir::{flags, Pool, Statement};
use model::{Permissions, Role};

const GET_ROLES: &str = r"
SELECT *
FROM ATLAS_ROLE
";

const GET_ROLE: &str = r"
SELECT AR.*
FROM ATLAS_ROLE AR
WHERE AR.ID = :roleid
";

const FIND_ROLES_BY_USER_ID: &str = r"
SELECT AR.*
FROM ATLAS_USER U
JOIN ATLAS_USER_ROLE AUR ON U.ID = AUR.USER_ID
JOIN ATLAS_ROLE AR ON AUR.ROLE_ID = AR.ID
WHERE U.ID = :userid
";

fn fetch_roles(stmt: &Statement, roles_vec: &mut Vec<Role>) -> Result<()> {
    // execute and fetch results
    stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

    let mut more_to_fetch = true;

    while more_to_fetch {
        let (_, num_rows_fetched, more_rows) = stmt.fetch_rows(10)?;
        more_to_fetch = more_rows;
        stmt.scroll(First, 0, 0)?;

        for i in 0..num_rows_fetched {
            stmt.fetch()?;
            let cols = stmt.get_num_query_columns()?;
            let mut role: Role = Default::default();
            let mut permissions: Permissions = Default::default();

            for j in 1..(cols + 1) {
                let (_val_type, val_data) = stmt.get_query_value(j)?;

                match j {
                    2 => role.set_rolename(val_data.get_string()),
                    3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 => {
                        permissions.set_at(j, val_data.get_boolean());
                    }
                    _ => {}
                }
            }
            role.set_permissions(permissions);
            roles_vec.push(role);

            if i < (num_rows_fetched - 1) {
                stmt.scroll(Next, 0, 0)?;
            }
        }
    }

    Ok(())
}

pub fn get_roles(pool: &Pool) -> Result<Vec<Role>> {
    let mut roles_vec = Vec::new();
    let conn = pool.acquire_connection(None, None, None)?;

    {
        // Prepare the statement
        let stmt = conn.prepare_stmt(Some(GET_ROLES), None, false)?;

        // execute and fetch results
        stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;
        fetch_roles(&stmt, &mut roles_vec)?;
    }

    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    Ok(roles_vec)
}

pub fn get_role_by_id(pool: &Pool, id: u64) -> Result<Role> {
    let mut roles_vec = Vec::new();
    let conn = pool.acquire_connection(None, None, None)?;

    {
        // Prepare the statement
        let stmt = conn.prepare_stmt(Some(GET_ROLE), None, false)?;

        // Setup the :roleid bind.
        let role_id_var = conn.new_var(Number, Uint64, 1, 0, false, false)?;
        let role_id_data = role_id_var.get_data()?;

        if role_id_data.len() == 1 {
            role_id_data[0].is_null = 0;
            role_id_data[0].value.as_uint_64 = id;
            stmt.bind_by_pos(1, &role_id_var)?;

            // execute and fetch results
            stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

            fetch_roles(&stmt, &mut roles_vec)?;
        } else {
            return Err(ErrorKind::InvalidVar.into());
        }
    }

    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    if roles_vec.len() == 1 {
        Ok(roles_vec[0].clone())
    } else {
        Err(ErrorKind::TooManyResults.into())
    }
}

pub fn find_roles_by_user_id(pool: &Pool, id: u64) -> Result<Vec<Role>> {
    let mut roles_vec = Vec::new();
    let conn = pool.acquire_connection(None, None, None)?;

    {
        let stmt = conn.prepare_stmt(Some(FIND_ROLES_BY_USER_ID), None, false)?;

        // Setup the :userid bind.
        let user_id_var = conn.new_var(Number, Uint64, 1, 0, false, false)?;
        let user_id_data = user_id_var.get_data()?;

        if user_id_data.len() == 1 {
            user_id_data[0].is_null = 0;
            user_id_data[0].value.as_uint_64 = id;
            stmt.bind_by_pos(1, &user_id_var)?;

            // execute and fetch results
            stmt.execute(flags::DPI_MODE_EXEC_DEFAULT)?;

            fetch_roles(&stmt, &mut roles_vec)?;
        } else {
            return Err(ErrorKind::InvalidVar.into());
        }
    }

    conn.commit()?;
    conn.close(flags::DPI_MODE_CONN_CLOSE_DEFAULT, None)?;

    Ok(roles_vec)
}
