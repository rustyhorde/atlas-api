//! Roles Model
use error::Result;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request};
use utils;

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct Permissions {
    read_users: bool,
    create_user: bool,
    read_user: bool,
    update_user: bool,
    delete_user: bool,
    read_roles: bool,
    create_role: bool,
    read_role: bool,
    update_role: bool,
    delete_role: bool,
    read_user_roles: bool,
    create_user_role: bool,
    read_user_role: bool,
    update_user_role: bool,
    delete_user_role: bool,
}

impl Permissions {
    pub fn set_at(&mut self, col: u32, val: bool) -> &mut Self {
        match col {
            3 => self.read_users = val,
            4 => self.create_user = val,
            5 => self.read_user = val,
            6 => self.update_user = val,
            7 => self.delete_user = val,
            8 => self.read_roles = val,
            9 => self.create_role = val,
            10 => self.read_role = val,
            11 => self.update_role = val,
            12 => self.delete_role = val,
            13 => self.read_user_roles = val,
            14 => self.create_user_role = val,
            15 => self.read_user_role = val,
            16 => self.update_user_role = val,
            17 => self.delete_user_role = val,
            _ => {}
        }
        self
    }
}

#[derive(
    Clone,
    Debug,
    Default,
    Deserialize,
    Eq,
    Getters,
    Hash,
    Ord,
    PartialEq,
    PartialOrd,
    Serialize,
    Setters,
)]
pub struct Role {
    #[get]
    #[set = "pub"]
    rolename: String,
    #[get]
    #[set = "pub"]
    permissions: Permissions,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct PatchPermissions {
    read_users: Option<bool>,
    create_user: Option<bool>,
    read_user: Option<bool>,
    update_user: Option<bool>,
    delete_user: Option<bool>,
    read_roles: Option<bool>,
    create_role: Option<bool>,
    read_role: Option<bool>,
    update_role: Option<bool>,
    delete_role: Option<bool>,
    read_user_roles: Option<bool>,
    create_user_role: Option<bool>,
    read_user_role: Option<bool>,
    update_user_role: Option<bool>,
    delete_user_role: Option<bool>,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct Patch {
    rolename: Option<String>,
    permissions: Option<PatchPermissions>,
}

macro_rules! role_request_guard {
    ($name:ident, $role:ident, $rolename:expr) => {
        pub struct $name(bool);

        /// Returns true if the given token represents an admin user.
        fn $role(header: &str) -> Result<bool> {
            let token_data = utils::get_token_data_from_header(header)?;
            let roles_vec = token_data.claims.rol();
            let mut has_role = false;
            for role in roles_vec {
                if role.rolename() == $rolename {
                    has_role = true;
                    break;
                }
            }
            Ok(has_role)
        }

        impl<'a, 'r> FromRequest<'a, 'r> for $name {
            type Error = ();

            fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
                let keys: Vec<_> = request.headers().get("Authorization").collect();
                if keys.len() != 1 {
                    return Outcome::Failure((Status::BadRequest, ()));
                }

                let key = keys[0];
                match $role(key) {
                    Ok(has_role) => {
                        if has_role {
                            Outcome::Success($name(has_role))
                        } else {
                            Outcome::Forward(())
                        }
                    }
                    Err(_) => Outcome::Forward(()),
                }
            }
        }
    };
}

role_request_guard!(AdminUser, admin, "Admin");
role_request_guard!(User, user, "Base");

macro_rules! permission_request_guard {
    ($name:ident, $permission:ident) => {
        pub struct $name(bool);

        /// Returns true if the given token represents has the requested role.
        fn $permission(header: &str) -> Result<bool> {
            let token_data = utils::get_token_data_from_header(header)?;
            let roles_vec = token_data.claims.rol();
            let mut has_permission = false;
            for role in roles_vec {
                let permissions = role.permissions();

                if permissions.$permission {
                    has_permission = true;
                    break;
                }
            }
            Ok(has_permission)
        }

        impl<'a, 'r> FromRequest<'a, 'r> for $name {
            type Error = ();

            fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
                let keys: Vec<_> = request.headers().get("Authorization").collect();
                if keys.len() != 1 {
                    return Outcome::Failure((Status::BadRequest, ()));
                }

                let key = keys[0];
                match $permission(key) {
                    Ok(has_permission) => {
                        if has_permission {
                            Outcome::Success($name(has_permission))
                        } else {
                            Outcome::Forward(())
                        }
                    }
                    Err(_) => Outcome::Forward(()),
                }
            }
        }
    };
}

permission_request_guard!(ReadRoles, read_roles);
permission_request_guard!(CreateRole, create_role);
permission_request_guard!(ReadRole, read_role);
permission_request_guard!(UpdateRole, update_role);
permission_request_guard!(DeleteRole, delete_role);
permission_request_guard!(ReadUsers, read_users);
permission_request_guard!(CreateUser, create_user);
permission_request_guard!(ReadUser, read_user);
permission_request_guard!(UpdateUser, update_user);
permission_request_guard!(DeleteUser, delete_user);
permission_request_guard!(ReadUserRoles, read_user_roles);
permission_request_guard!(CreateUserRole, create_user_role);
permission_request_guard!(ReadUserRole, read_user_role);
permission_request_guard!(UpdateUserRole, update_user_role);
permission_request_guard!(DeleteUserRole, delete_user_role);
