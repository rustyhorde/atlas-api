//! Authentication/Authorization Models
use chrono::Utc;
use error::{ErrorKind, Result};
use jsonwebtoken::TokenData;
use model::Role;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request};
use utils;

const SECONDS_PER_30_MINUTES: i64 = 30 * 60;

lazy_static! {
    #[derive(Copy, Clone, Debug)]
    pub static ref ISSUER: String = {
        format!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
    };
    #[derive(Copy, Clone, Debug)]
    pub static ref URL_ENC_ISSUER: String = {
        format!("{}%20{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
    };
}

/// Authentication struct
#[derive(Deserialize, Getters, Serialize)]
pub struct Credentials {
    /// User email address
    #[get = "pub"]
    username: String,
    /// User password
    #[get = "pub"]
    password: String,
}

#[derive(Clone, Debug, Deserialize, Getters, Serialize, Setters)]
pub struct Claims {
    #[set = "pub"]
    iss: String,
    #[get = "pub"]
    #[set = "pub"]
    sub: String,
    // #[set = "pub"]
    iat: i64,
    // #[set = "pub"]
    nbf: i64,
    // #[set = "pub"]
    exp: i64,
    // Atlas User ID
    #[set = "pub"]
    aid: u64,
    // Is Two-Factor Authentication required?
    #[set = "pub"]
    tfa: bool,
    // Atlas User Roles
    #[get = "pub"]
    #[set = "pub"]
    rol: Vec<Role>,
}

impl Default for Claims {
    fn default() -> Self {
        let now = Utc::now().timestamp();
        // Set expiration 30 minutes from now.
        let exp = now + SECONDS_PER_30_MINUTES;
        Self {
            iss: String::new(),
            sub: String::new(),
            iat: now,
            nbf: now,
            exp: exp,
            aid: 0,
            tfa: false,
            rol: Vec::new(),
        }
    }
}

#[derive(Getters)]
pub struct PlainJwt {
    #[get = "pub"]
    claims: Claims,
}

/// Returns true if the given `header` is a valid JSON Web Token.
fn is_valid(header: &str) -> Result<TokenData<Claims>> {
    Ok(utils::get_token_data_from_header(header)?)
}

impl<'a, 'r> FromRequest<'a, 'r> for PlainJwt {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
        if let Some(atlas_session) = request.cookies().get_private("atlas_session") {
            let user_id = atlas_session.value();
            let keys: Vec<_> = request.headers().get("Authorization").collect();
            if keys.len() != 1 {
                return Outcome::Failure((Status::BadRequest, ()));
            }

            let key = keys[0];
            match is_valid(key) {
                Ok(token_data) => {
                    let token_user_id = token_data.claims.aid;

                    if user_id == token_user_id.to_string() {
                        Outcome::Success(Self {
                            claims: token_data.claims,
                        })
                    } else {
                        Outcome::Forward(())
                    }
                }
                Err(_) => Outcome::Forward(()),
            }
        } else {
            Outcome::Forward(())
        }
    }
}

#[derive(Getters)]
pub struct TfaJwt {
    #[get = "pub"]
    claims: Claims,
}

/// Returns true if the given `header` is a valid JSON Web Token.
fn is_valid_tfa(header: &str) -> Result<TokenData<Claims>> {
    let token_data = utils::get_token_data_from_header(header)?;
    if token_data.claims.tfa {
        Err(ErrorKind::TfaRequired.into())
    } else {
        Ok(token_data)
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for TfaJwt {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
        if let Some(atlas_session) = request.cookies().get_private("atlas_session") {
            let user_id = atlas_session.value();
            let keys: Vec<_> = request.headers().get("Authorization").collect();
            if keys.len() != 1 {
                return Outcome::Failure((Status::BadRequest, ()));
            }

            let key = keys[0];
            match is_valid_tfa(key) {
                Ok(token_data) => {
                    let token_user_id = token_data.claims.aid;

                    if user_id == token_user_id.to_string() {
                        Outcome::Success(Self {
                            claims: token_data.claims,
                        })
                    } else {
                        Outcome::Forward(())
                    }
                }
                Err(_) => Outcome::Forward(()),
            }
        } else {
            Outcome::Forward(())
        }
    }
}

#[derive(Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Action {
    CheckTotp,
    DisableTfa,
    EnableTfa,
}

#[derive(Deserialize, Getters, Serialize)]
pub struct Patch {
    #[get = "pub"]
    action: Action,
    auth: Option<Credentials>,
    #[get = "pub"]
    totp: Option<u64>,
}

#[derive(Default, Deserialize, Serialize, Setters)]
pub struct TotpKey {
    #[set = "pub"]
    key: String,
    #[set = "pub"]
    filename: String,
}

#[derive(Default, Deserialize, Getters, Serialize, Setters)]
pub struct Response {
    #[set = "pub"]
    token: Option<String>,
    #[set = "pub"]
    id: Option<u64>,
    #[set = "pub"]
    totp: Option<TotpKey>,
}
