//! `Env` model
use error::{Error, ErrorKind, Result};
use std::convert::TryFrom;
use std::fmt;

/// `Env` enum
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Env {
    Dev,
    Stage,
    Prod,
}

impl Env {
    pub fn is_dev(&self) -> bool {
        match *self {
            Env::Dev => true,
            _ => false,
        }
    }
}

impl<'a> TryFrom<&'a str> for Env {
    type Error = Error;
    fn try_from(env_str: &str) -> Result<Self> {
        match env_str {
            "dev" | "development" => Ok(Env::Dev),
            "stage" | "staging" => Ok(Env::Stage),
            "prod" | "production" => Ok(Env::Prod),
            _ => Err(ErrorKind::InvalidEnv(env_str.to_string()).into()),
        }
    }
}

impl<'a> TryFrom<Option<&'a str>> for Env {
    type Error = Error;

    fn try_from(env_opt: Option<&str>) -> Result<Self> {
        if let Some(env_str) = env_opt {
            TryFrom::try_from(env_str)
        } else {
            let err_str = env_opt.unwrap_or_else(|| "None").to_string();
            Err(ErrorKind::InvalidEnv(err_str).into())
        }
    }
}

impl Into<String> for Env {
    fn into(self) -> String {
        Into::<&str>::into(self).to_string()
    }
}

impl<'a> Into<&'a str> for Env {
    fn into(self) -> &'a str {
        match self {
            Env::Dev => "dev",
            Env::Stage => "stage",
            Env::Prod => "prod",
        }
    }
}

impl fmt::Display for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", Into::<&str>::into(self.clone()))
    }
}
