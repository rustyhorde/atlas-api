//! Roles REST API
use db::role;
use error::{ErrorKind, Result};
use mimir::Pool;
use model::{CreateRole, DeleteRole, ReadRole, ReadRoles, Role, RolePatch, TfaJwt, UpdateRole};
use rocket::State;
use rocket_contrib::Json;

#[derive(Default, Serialize)]
struct RolesResponse {
    id: Option<u64>,
    role: Option<Role>,
    roles: Option<Vec<Role>>,
}

/// Get all roles.  Requires `read_roles` permissions.
#[get("/roles")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn get(pool: State<Pool>, _jwt: TfaJwt, _read_roles: ReadRoles) -> Result<Json<RolesResponse>> {
    let roles = role::get_roles(&pool)?;
    Ok(Json(RolesResponse {
        roles: Some(roles),
        ..Default::default()
    }))
}

/// Create a new role if it doesn't exist already.
#[post("/roles", data = "<role>", format = "application/json")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn post(
    role: Json<Role>,
    _pool: State<Pool>,
    _jwt: TfaJwt,
    _create_role: CreateRole,
) -> Result<Json<RolesResponse>> {
    let _ = role;
    Err(ErrorKind::NotImplemented.into())
}

/// Get a role by id: Requires `read_role` permission.argon2rs
#[get("/roles/<id>")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn get_by_id(
    id: u64,
    pool: State<Pool>,
    _jwt: TfaJwt,
    _read_role: ReadRole,
) -> Result<Json<RolesResponse>> {
    let role = role::get_role_by_id(&pool, id)?;
    Ok(Json(RolesResponse {
        role: Some(role),
        ..Default::default()
    }))
}

/// Update an entire role if it exists
#[put("/roles/<id>", data = "<role>", format = "application/json")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn put(
    id: u64,
    role: Json<Role>,
    _pool: State<Pool>,
    _jwt: TfaJwt,
    _update_role: UpdateRole,
) -> Result<Json<RolesResponse>> {
    let _ = role;
    let _ = id;
    Err(ErrorKind::NotImplemented.into())
}

/// Update parts of a role if it exists
#[patch("/roles/<id>", data = "<role>", format = "application/json")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn patch(
    id: u64,
    role: Json<RolePatch>,
    _pool: State<Pool>,
    _jwt: TfaJwt,
    _update_role: UpdateRole,
) -> Result<Json<RolesResponse>> {
    let _ = role;
    let _ = id;
    Err(ErrorKind::NotImplemented.into())
}

/// Delete a role if it exists
#[delete("/roles/<id>")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn delete(
    id: u64,
    _pool: State<Pool>,
    _jwt: TfaJwt,
    _delete_role: DeleteRole,
) -> Result<Json<RolesResponse>> {
    let _ = id;
    Err(ErrorKind::NotImplemented.into())
}
