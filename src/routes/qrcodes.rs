//! QR Codes REST API
use error::Result;
use model::PlainJwt;
use rocket::response::NamedFile;
use std::path::{Path, PathBuf};

/// Get a QR Code by filename.
#[get("/qrcodes/<file..>")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn get(file: PathBuf, _jwt: PlainJwt) -> Result<NamedFile> {
    Ok(NamedFile::open(Path::new("static/").join(file))?)
}
