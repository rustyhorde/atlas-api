//! Authentication REST API
use argon2rs;
use base32::{decode, encode, Alphabet};
use chrono::Utc;
use db::{role, user};
use error::{ErrorKind, Result};
use image::Luma;
use jsonwebtoken::{self, Algorithm, Header};
use mimir::Pool;
use model::{
    Action, AuthPatch, Claims, Credentials, Env, PlainJwt, Response, TfaJwt, TotpKey, ISSUER,
    URL_ENC_ISSUER,
};
use oath::{totp_raw_custom_time, totp_raw_now, HashType};
use qrcode::QrCode;
use rand::{self, Rng};
use rocket::http::{Cookie, Cookies};
use rocket::State;
use rocket_contrib::Json;
use slog::Logger;
use std::convert::TryFrom;
use std::env;
use std::path::PathBuf;
use utils::{self, stdout, MsgKind};

/// Post endpoint for `Auths`
#[post("/auths", data = "<auth>", format = "application/json")]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn post(
    env: State<Env>,
    pool: State<Pool>,
    log: State<Logger>,
    auth: Json<Credentials>,
    mut cookies: Cookies,
) -> Result<Json<Response>> {
    let mut unhashed: String = (*auth.username()).clone();
    unhashed.push_str(auth.password());

    let hash = argon2rs::argon2i_simple(&unhashed, &env::var("HASH_SALT")?);
    let hash_str: String = utils::to_hex_str(&hash);
    if env.is_dev() {
        stdout(&MsgKind::Security, &format!("Hash: {}", &hash_str))?;
    }

    if let Ok((id, tfa)) = user::find_user_by_auth_hash(&pool, &hash_str) {
        if env.is_dev() {
            stdout(
                &MsgKind::Security,
                "User authentication hash matched, generating token",
            )?;
        }

        let mut claims: Claims = Default::default();
        claims.set_iss(ISSUER.to_string());
        claims.set_sub(hash_str);
        claims.set_aid(id);
        claims.set_tfa(tfa);
        if let Ok(roles) = role::find_roles_by_user_id(&pool, id) {
            claims.set_rol(roles);
        }

        let mut header: Header = Default::default();
        header.alg = Algorithm::HS512;
        let secret = env::var("JWT_SECRET")?;
        let token = jsonwebtoken::encode(&header, &claims, secret.as_bytes())?;

        if env.is_dev() {
            let msg = &format!("Token: {}", token);
            info!(log, "{}", msg);
            stdout(&MsgKind::Security, msg)?;
        }

        if cookies.get_private("atlas_session").is_none() {
            let mut session_cookie = Cookie::new("atlas_session", id.to_string());
            session_cookie.set_http_only(true);
            session_cookie.set_path("/api/v1");
            session_cookie.make_permanent();

            if env.is_dev() {
                let msg = &format!("Creating Cookie: {}", session_cookie);
                info!(log, "{}", msg);
                stdout(&MsgKind::Security, msg)?;
            }

            cookies.add_private(session_cookie);
        } else if env.is_dev() {
            let msg = "Cookie Exists";
            info!(log, "{}", msg);
            stdout(&MsgKind::Security, msg)?;
        }

        let mut resp: Response = Default::default();
        resp.set_token(Some(token));
        resp.set_id(Some(id));

        Ok(Json(resp))
    } else {
        Err(ErrorKind::Auth.into())
    }
}

fn check_totp(
    jwt: &PlainJwt,
    env: &Env,
    pool: &Pool,
    log: &Logger,
    id: u64,
    given_totp: u64,
) -> Result<Json<Response>> {
    use std::io::{self, Write};
    let sub = jwt.claims().sub();
    writeln!(io::stderr(), "ID: {}", id)?;
    writeln!(io::stderr(), "Subject: {}", sub)?;
    let sk = user::get_tfa_sk(pool, id, sub)?;
    writeln!(io::stderr(), "SK: {}", sk)?;
    let decoded_sk =
        decode(Alphabet::RFC4648 { padding: false }, &sk).ok_or_else(|| ErrorKind::Decode)?;
    let now = Utc::now().timestamp();
    let previous = TryFrom::try_from(now - 30)?;
    let next = TryFrom::try_from(now + 30)?;
    let previous_totp = totp_raw_custom_time(&decoded_sk, 6, 0, 30, previous, &HashType::SHA1);
    let current_totp = totp_raw_now(&decoded_sk, 6, 0, 30, &HashType::SHA1);
    let next_totp = totp_raw_custom_time(&decoded_sk, 6, 0, 30, next, &HashType::SHA1);

    if env.is_dev() {
        let msg = &format!(
            "Given: {}, Previous: {}, Current: {}, Next: {}",
            given_totp, previous_totp, current_totp, next_totp
        );
        info!(log, "{}", msg);
        stdout(&MsgKind::Security, msg)?;
    }

    if given_totp == current_totp || given_totp == previous_totp || given_totp == next_totp {
        let mut new_claims = jwt.claims().clone();
        new_claims.set_tfa(false);

        let mut header: Header = Default::default();
        header.alg = Algorithm::HS512;
        let secret = env::var("JWT_SECRET")?;
        let token = jsonwebtoken::encode(&header, &new_claims, secret.as_bytes())?;

        let mut resp: Response = Default::default();
        resp.set_token(Some(token));
        resp.set_id(Some(id));

        Ok(Json(resp))
    } else {
        Err(ErrorKind::Auth.into())
    }
}

fn enable_tfa(jwt: &PlainJwt, pool: &Pool, id: u64) -> Result<Json<Response>> {
    let sub = jwt.claims().sub();
    let mut v = vec![0; 32];
    let mut rand_filename = vec![0; 32];
    let mut rng = rand::thread_rng();

    for x in &mut v {
        *x = rng.gen();
    }

    for x in &mut rand_filename {
        *x = rng.gen();
    }
    let secret = encode(Alphabet::RFC4648 { padding: false }, &v);
    let filename = encode(Alphabet::RFC4648 { padding: false }, &rand_filename);
    let uri = format!(
        "otpauth://totp/{}:{}?secret={}&issuer={}&algorithm=SHA1&digits=6&period=30",
        *URL_ENC_ISSUER, sub, secret, *URL_ENC_ISSUER
    );
    // Encode some data into bits.
    if let Ok(code) = QrCode::new(uri.as_bytes()) {
        // Render the bits into an image.
        let image = code.render::<Luma<u8>>().build();
        // Save the image.
        let mut local_fn = PathBuf::from("static/");
        local_fn.push(&filename);
        local_fn.set_extension("png");

        image.save(local_fn)?;
        user::enable_tfa(pool, id, sub, &secret, &filename)?;
    } else {
        return Err(ErrorKind::QrCode.into());
    }

    let mut resp: Response = Default::default();
    let mut totp_key: TotpKey = Default::default();
    totp_key.set_key(secret);
    totp_key.set_filename(filename);

    resp.set_totp(Some(totp_key));

    Ok(Json(resp))
}

/// Patch endpoint for `Auths` that require TFA.
#[patch(
    "/auths/<id>",
    data = "<payload>",
    format = "application/json",
    rank = 1
)]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn patch_tfa(
    id: u64,
    _env: State<Env>,
    _pool: State<Pool>,
    _log: State<Logger>,
    jwt: TfaJwt,
    payload: Json<AuthPatch>,
) -> Result<Json<Response>> {
    let _id = id;
    let _sub = jwt.claims().sub();
    match *payload.action() {
        Action::DisableTfa => Ok(Json(Default::default())),
        _ => Err(ErrorKind::InvalidAction.into()),
    }
}

/// Patch endpoint for `Auths` that don't require TFA.
#[patch(
    "/auths/<id>",
    data = "<payload>",
    format = "application/json",
    rank = 2
)]
#[cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]
fn patch_plain(
    id: u64,
    env: State<Env>,
    pool: State<Pool>,
    log: State<Logger>,
    jwt: PlainJwt,
    payload: Json<AuthPatch>,
) -> Result<Json<Response>> {
    match *payload.action() {
        Action::CheckTotp => {
            if let Some(totp) = *payload.totp() {
                check_totp(&jwt, &env, &pool, &log, id, totp)
            } else {
                Err(ErrorKind::TfaRequired.into())
            }
        }
        Action::EnableTfa => enable_tfa(&jwt, &pool, id),
        _ => Err(ErrorKind::TfaRequired.into()),
    }
}
