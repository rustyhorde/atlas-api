//! OPTIONS route handler
use rocket::handler::Outcome;
use rocket::http::Method;
use rocket::{Data, Request, Route};

/// Returns "catch all" OPTIONS routes that you can mount to catch all OPTIONS request. Only works
/// if you have put a `Cors` struct into Rocket's managed state.
///
/// This route has very high rank (and therefore low priority) of
/// [max value](https://doc.rust-lang.org/nightly/std/primitive.isize.html#method.max_value)
/// so you can define your own to override this route's behaviour.
///
/// See the documentation at the [crate root](index.html) for usage information.
pub fn catch_all_options_routes() -> Vec<Route> {
    vec![
        Route::ranked(
            isize::max_value(),
            Method::Options,
            "/",
            catch_all_options_route_handler,
        ),
        Route::ranked(
            isize::max_value(),
            Method::Options,
            "/<catch_all_options_route..>",
            catch_all_options_route_handler,
        ),
    ]
}

/// Handler for the "catch all options route"
fn catch_all_options_route_handler<'r>(request: &'r Request, _: Data) -> Outcome<'r> {
    Outcome::from(request, ())
}
